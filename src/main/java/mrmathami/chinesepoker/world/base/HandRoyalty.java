package mrmathami.chinesepoker.world.base;

public enum HandRoyalty {
	MIS_SET("Mis-set"),
	NORMAL("Normal"),
	SIX_PAIRS("Six pairs"),
	THREE_STRAIGHTS("Three straights"),
	THREE_FLUSHES("Three flushes"),
	COLOR_TWELVE("Twelve same color cards"),
	COLOR_THIRTEEN("Thirteen same color cards"),
	DRAGON_HAND("Dragon hand");

	public static final HandRoyalty[] values = HandRoyalty.values();
	private final String string;

	HandRoyalty(String string) {
		this.string = string;
	}

	boolean isWhiteWin() {
		return this.compareTo(NORMAL) > 0;
	}

	@Override
	public String toString() {
		return this.string;
	}
}
