package phanh.test;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public final class SortingScene extends Application {
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		Parent root = FXMLLoader.load(getClass().getResource("SortingScene.fxml"));
		primaryStage.setTitle("Hello world");
		primaryStage.setScene(new Scene(root, 800, 600));
		primaryStage.show();
	}
}
