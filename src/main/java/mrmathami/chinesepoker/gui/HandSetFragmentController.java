package mrmathami.chinesepoker.gui;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.ObjectExpression;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import mrmathami.chinesepoker.smallworld.base.*;

import java.net.URL;
import java.util.ResourceBundle;

public final class HandSetFragmentController implements Initializable {
	private static final Border BORDER = new Border(new BorderStroke(Color.RED, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderStroke.MEDIUM));

	@FXML private Label rectangle;
	@FXML private Button top_1;
	@FXML private Button top_2;
	@FXML private Button top_3;
	@FXML private Button mid_1;
	@FXML private Button mid_2;
	@FXML private Button mid_3;
	@FXML private Button mid_4;
	@FXML private Button mid_5;
	@FXML private Button bottom_1;
	@FXML private Button bottom_2;
	@FXML private Button bottom_3;
	@FXML private Button bottom_4;
	@FXML private Button bottom_5;

	private Button[] buttons;
	private int[] cards;
	private boolean isSwappable;
	private boolean isSelected;
	private int selectIndex;

	private static void setButtonCard(Button button, int card) {
		button.setText(Rank.toString(Card.getRank(card)) + "\n" + Suit.toString(Card.getSuit(card)));
		button.setTextFill(Suit.isRed(Card.getSuit(card)) ? Color.RED : Color.BLACK);
	}

	public void setSwappable(boolean swappable) {
		isSwappable = swappable;
	}

	public void setCards(HandSet handSet) {
		if (handSet.equals(HandSet.EMPTY)) {
			for (Button button : buttons) {
				button.setText("-\n-");
				button.setTextFill(Color.BLUE);
			}
			return;
		}
		final int[] top = Cards.toArray(handSet.getTop().getCards());
		final int[] mid = Cards.toArray(handSet.getMid().getCards());
		final int[] bottom = Cards.toArray(handSet.getBottom().getCards());
		this.cards = new int[buttons.length];
		setCards(
				top[0], top[1], top[2],
				mid[0], mid[1], mid[2], mid[3], mid[4],
				bottom[0], bottom[1], bottom[2], bottom[3], bottom[4]
		);
	}

	public void setCards(long cards) {
		setCards(Cards.toArray(cards));
	}

	public void setCards(int... cards) {
		this.cards = cards;
		for (int i = 0; i < buttons.length; i++) {
			setButtonCard(buttons[i], cards[i]);
		}
	}

	public HandSet getHandSet() {
		if (cards == null) return null;
		return HandSet.of(
				Cards.of(cards[0], cards[1], cards[2]),
				Cards.of(cards[3], cards[4], cards[5], cards[6], cards[7]),
				Cards.of(cards[8], cards[9], cards[10], cards[11], cards[12])
		);
	}

	@Override
	public void initialize(URL url, ResourceBundle rb) {
		this.buttons = new Button[]{
				top_1, top_2, top_3,
				mid_1, mid_2, mid_3, mid_4, mid_5,
				bottom_1, bottom_2, bottom_3, bottom_4, bottom_5
		};
		this.cards = new int[buttons.length];

		ObjectExpression<Font> fontObjectExpression =
				Bindings.createObjectBinding(
						() -> {
//							System.out.println("min = " + Math.min(rectangle.getWidth() / 3, rectangle.getHeight() / 4));
//							System.out.println("width = " + rectangle.getWidth() / 3);
//							System.out.println("height = " + rectangle.getHeight() / 4);
							return Font.font("Consolas", FontWeight.BOLD, Math.min(rectangle.getWidth() / 3, rectangle.getHeight() / 4));
						},
						rectangle.widthProperty(),
						rectangle.heightProperty()
				);
		for (Button button : this.buttons) {
			button.fontProperty().bind(fontObjectExpression);
		}
	}

	private int sourceToIndex(Object object) {
		for (int i = 0; i < buttons.length; i++) {
			if (object.equals(buttons[i])) return i;
		}
		return -1;
	}

	public void onAction(ActionEvent event) {
		if (!isSwappable) return;

		final int selectSwapIndex = sourceToIndex(event.getSource());
		if (selectSwapIndex < 0) return;
		if (isSelected) {
			// do swap
			final int card = cards[selectIndex];
			cards[selectIndex] = cards[selectSwapIndex];
			cards[selectSwapIndex] = card;

			setButtonCard(buttons[selectIndex], cards[selectIndex]);
			setButtonCard(buttons[selectSwapIndex], cards[selectSwapIndex]);
			buttons[selectIndex].setBorder(Border.EMPTY);

			this.isSelected = false;
		} else {
			this.selectIndex = selectSwapIndex;
			buttons[selectIndex].setBorder(BORDER);
			this.isSelected = true;
		}
	}
}


