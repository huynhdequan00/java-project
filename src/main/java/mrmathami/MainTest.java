//package mrmathami;
//
//import mrmathami.chinesepoker.smallworld.computer.ComputerBrain;
//import mrmathami.chinesepoker.smallworld.game.Game;
//import mrmathami.chinesepoker.smallworld.game.GameInstance;
//import mrmathami.chinesepoker.smallworld.game.Player;
//
//import java.util.concurrent.TimeUnit;
//
//public class MainTest {
//
//	public static void main(String[] args) throws ClassNotFoundException {
//		Class.forName("mrmathami.chinesepoker.smallworld.base.Cards");
//	}
//
//	public static void maxin(String[] args) throws ClassNotFoundException, InterruptedException {
//		Class.forName("mrmathami.chinesepoker.world.base.CardSet");
//
//		// write your code here
//		long startTime = System.nanoTime();
//
///*
//		TableHands table = TableHands.generateTableHands(new SecureRandom(new byte[]{1, 2, 3, 4, 5, 6, 7, 8}));
//		Timer timer = Timer.generateTableHands(30, TimeUnit.SECONDS);
//
//		System.out.println(table);
//
//		System.out.println(table.get(PlayerOrder.PLAYER_1));
//		ComputerBrain.play(table, PlayerOrder.PLAYER_1, timer, result -> {
//		});
//		System.out.println(table.get(PlayerOrder.PLAYER_2));
//		ComputerBrain.play(table, PlayerOrder.PLAYER_2, timer, result -> {
//		});
//		System.out.println(table.get(PlayerOrder.PLAYER_3));
//		ComputerBrain.play(table, PlayerOrder.PLAYER_3, timer, result -> {
//		});
//		System.out.println(table.get(PlayerOrder.PLAYER_4));
//		ComputerBrain.play(table, PlayerOrder.PLAYER_4, timer, result -> {
//		});
//		//ScoredMap.initialize();
//*/
//
//		Player player_A = Player.of(ComputerBrain::play, 0);
//		Player player_B = Player.of(ComputerBrain::play, 0);
//		Player player_C = Player.of(ComputerBrain::play, 0);
//		Player player_D = Player.of(ComputerBrain::play, 0);
//
//		Game game = Game.of(3, TimeUnit.SECONDS,
//				player_A,
//				player_B,
//				player_C,
//				player_D
//		);
//
//		GameInstance gameInstance = game.createInstance(() -> {
//			System.out.println("onStartCallback");
//		}, () -> {
//			System.out.println("onTickCallback");
//		}, () -> {
//			System.out.println("onStopCallback");
//		});
////		ComputerBrain.play(gameInstance.getTableHands(), PlayerOrder.PLAYER_2, null, result -> {});
//
//		gameInstance.start();
//		gameInstance.waitUntilFinish();
//
//		System.out.println(gameInstance.getHandSets());
//		System.out.println(gameInstance.getDeltas());
//
//		//HandSet handSet = HandSet.generateTableHands(Cards.generateTableHands(Card.CARD_8D, Card.CARD_8S, Card.CARD_AS), Cards.generateTableHands(Card.CARD_4C, Card.CARD_7H, Card.CARD_7D, Card.CARD_9S, Card.CARD_9D), Cards.generateTableHands(Card.CARD_4H, Card.CARD_6S, Card.CARD_6C, Card.CARD_6D, Card.CARD_6H));
//
//		long endTime = System.nanoTime();
//		long totalTime = endTime - startTime;
//		System.out.println((double) totalTime / 1000000.0);
//	}
//}
