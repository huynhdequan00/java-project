package mrmathami.chinesepoker.world.game;

import mrmathami.utilities.enumerate.EnumArray;
import mrmathami.utilities.enumerate.ReadOnlyEnumArray;

public final class TableDeltas extends ReadOnlyEnumArray<PlayerOrder, Long> {
	private TableDeltas(EnumArray<PlayerOrder, Long> enumArray) {
		super(enumArray);
	}

	public static TableDeltas of(EnumArray<PlayerOrder, Long> enumArray) {
		return new TableDeltas(enumArray);
	}
}