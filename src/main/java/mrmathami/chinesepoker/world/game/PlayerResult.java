package mrmathami.chinesepoker.world.game;

import mrmathami.chinesepoker.world.base.HandSet;

@FunctionalInterface
public interface PlayerResult {
	/**
	 * For you to store your result. Can be called multiple times to change the result before the timeout.
	 * @param result result
	 * @throws InterruptedException timeout exception
	 */
	void setResult(HandSet result) throws InterruptedException;
}
