package mrmathami.chinesepoker.gui;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import mrmathami.chinesepoker.smallworld.computer.ComputerBrain;
import mrmathami.chinesepoker.smallworld.game.*;
import mrmathami.utilities.ThreadFactoryBuilder;
import mrmathami.utilities.Timer;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class GUIController implements Initializable {
	private final Game game;

	private final ScheduledExecutorService scheduledExecutor = Executors.newSingleThreadScheduledExecutor(
			new ThreadFactoryBuilder()
					.setNamePrefix("GUIControllerScheduledExecutor")
					.setDaemon(true)
					.setPriority(Thread.MAX_PRIORITY)
					.build()
	);
	@FXML public GridPane mainScene;
	@FXML public AnchorPane mainSceneHider;
	@FXML public MainSceneController mainSceneController;
	@FXML public GridPane sortingScene;
	@FXML public AnchorPane sortingSceneHider;
	@FXML public SortingSceneController sortingSceneController;


	private HumanBrain humanBrain;
	private GameInstance gameInstance;
	private Timer timer;
	private int gameCount;

	public GUIController() {
		this.gameCount = 0;
		this.humanBrain = new HumanBrain();
		this.game = Game.of(60, TimeUnit.SECONDS,
				Player.of(ComputerBrain::play_against_last, 10000),
				Player.of(ComputerBrain::play_against_last, 10000),
				Player.of(ComputerBrain::play_against_last, 10000),
				Player.of(ComputerBrain::play, 10000)
//				Player.of(humanBrain, 10000)
		);
	}

	@Override
	public void initialize(URL url, ResourceBundle rb) {
		mainSceneController.setGuiController(this);
		sortingSceneController.setGuiController(this);
		sortingSceneHider.setVisible(false);
	}


	public void onStartAction() {
		this.gameInstance = game.createInstance(this::onStartCallback, this::onTickCallback, this::onFinishCallback);
		gameInstance.start();
		this.gameCount += 1;
	}

	public void onFinishAction() {
		humanBrain.setResult(sortingSceneController.mainHandController.getHandSet());
		humanBrain.interrupt();

		//
		scheduledExecutor.schedule(() -> Platform.runLater(this::onStartAction), 1, TimeUnit.SECONDS);
	}

	private void onStartCallback() {
		mainSceneHider.setVisible(false);
		sortingSceneHider.setVisible(true);
		this.timer = Timer.of(60, TimeUnit.SECONDS);
		sortingSceneController.countDown.setText(timer.getRemainingTime(TimeUnit.SECONDS) + "s");
		final TableHands tableHands = gameInstance.getTableHands();
		sortingSceneController.firstHandController.setCards(tableHands.get(PlayerOrder.PLAYER_1));
		sortingSceneController.secondHandController.setCards(tableHands.get(PlayerOrder.PLAYER_2));
		sortingSceneController.thirdHandController.setCards(tableHands.get(PlayerOrder.PLAYER_3));
		sortingSceneController.mainHandController.setCards(tableHands.get(PlayerOrder.PLAYER_4));
	}

	private void onTickCallback() {
		Platform.runLater(() -> {
			sortingSceneController.countDown.setText(timer.getRemainingTime(TimeUnit.SECONDS) + "s");
		});
	}

	private void onFinishCallback() {
		onFinishAction();
		Platform.runLater(() -> {
			sortingSceneHider.setVisible(false);
			mainSceneHider.setVisible(true);
			final TableHandSets tableHandSets = gameInstance.getHandSets();
			mainSceneController.firstHandController.setCards(tableHandSets.get(PlayerOrder.PLAYER_1));
			mainSceneController.secondHandController.setCards(tableHandSets.get(PlayerOrder.PLAYER_2));
			mainSceneController.thirdHandController.setCards(tableHandSets.get(PlayerOrder.PLAYER_3));
			mainSceneController.mainHandController.setCards(tableHandSets.get(PlayerOrder.PLAYER_4));
			final TableDeltas tableDeltas = gameInstance.getDeltas();
			mainSceneController.firstDeltaScore.setText(tableDeltas.get(PlayerOrder.PLAYER_1).toString());
			mainSceneController.secondDeltaScore.setText(tableDeltas.get(PlayerOrder.PLAYER_2).toString());
			mainSceneController.thirdDeltaScore.setText(tableDeltas.get(PlayerOrder.PLAYER_3).toString());
			mainSceneController.mainDeltaScore.setText(tableDeltas.get(PlayerOrder.PLAYER_4).toString());
			final Players players = game.getPlayers();
			mainSceneController.firstScore.setText(Long.toString(players.get(PlayerOrder.PLAYER_1).getScore()));
			mainSceneController.secondScore.setText(Long.toString(players.get(PlayerOrder.PLAYER_2).getScore()));
			mainSceneController.thirdScore.setText(Long.toString(players.get(PlayerOrder.PLAYER_3).getScore()));
			mainSceneController.mainScore.setText(Long.toString(players.get(PlayerOrder.PLAYER_4).getScore()));

			mainSceneController.start.setText("Start game " + gameCount);
		});
	}
}


