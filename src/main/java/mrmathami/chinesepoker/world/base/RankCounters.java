package mrmathami.chinesepoker.world.base;

import java.util.Arrays;

final class RankCounters implements Comparable<RankCounters> {
	private final int[] counters;

	private RankCounters(int[] counters) {
		this.counters = counters;
	}

	static RankCounters of(Cards cards) {
		final int[] tmpCounters = new int[cards.size()];
		int size = 0;
		Rank[] values = Rank.values;
		for (int ordinal = 0; ordinal < values.length; ordinal++) {
			final int count = cards.countRank(values[ordinal]);
			if (count > 0) tmpCounters[size++] = ordinal | (count << 16);
		}

		int[] counters = new int[size];
		for (int counterIndex = 0; counterIndex < size; counterIndex++) {
			int maxIndex = 0;
			for (int tmpIndex = 0; tmpIndex < size; tmpIndex++) {
				if (tmpCounters[maxIndex] < tmpCounters[tmpIndex]) maxIndex = tmpIndex;
			}
			counters[counterIndex] = tmpCounters[maxIndex];
			tmpCounters[maxIndex] = 0;
		}
		return new RankCounters(counters);
	}

	int size() {
		return counters.length;
	}

	int getCount(int index) {
		return counters[index] >> 16;
	}

	int getRankOrdinal(int index) {
		return counters[index] & 0xFFFF;
	}

	Rank getRank(int index) {
		return Rank.values[getRankOrdinal(index)];
	}

	@Override
	public int compareTo(RankCounters rankCounters) {
		return Arrays.compare(this.counters, rankCounters.counters);
	}
}
