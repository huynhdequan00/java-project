package mrmathami.chinesepoker.smallworld.base;

import java.util.Arrays;

public final class CardSet implements Comparable<CardSet> {
	private static final CardSet[] CARD_SETS;

	static {
		CARD_SETS = createCardSets();
		System.gc();
	}

	private final long cards;
	private final int scoreRoyaltyStraight;

	private CardSet(long cards, int royalty, boolean isStraight, int score) {
		this.cards = cards;
		this.scoreRoyaltyStraight = (isStraight ? 1 : 0) + (royalty << 1) + (score << 16);
//		this.royalty = royalty;
//		this.isStraight = isStraight;
//		this.score = score;
	}

	public static CardSet of(long cards) {
		assert Cards.size(cards) == 3 || Cards.size(cards) == 5;
		return getByPrimitive(cards);
	}

	public static CardSet of(int... cards) {
		assert cards.length == 3 || cards.length == 5;
		return getByPrimitive(Cards.of(cards));
	}

//	private static CardSet[] createCardSets() {
//		final ArrayList<InternalCardSet> internalCardSets = new ArrayList<>(2621060);
//		for (int cardA = 0; cardA < Card.LENGTH; cardA++) {
//			for (int cardB = cardA + 1; cardB < Card.LENGTH; cardB++) {
//				for (int cardC = cardB + 1; cardC < Card.LENGTH; cardC++) {
//					// add Trinity
//					internalCardSets.add(InternalCardSet.of(cardA, cardB, cardC));
//
//					for (int cardD = cardC + 1; cardD < Card.LENGTH; cardD++) {
//						for (int cardE = cardD + 1; cardE < Card.LENGTH; cardE++) {
//							// add Quintet
//							internalCardSets.add(InternalCardSet.of(cardA, cardB, cardC, cardD, cardE));
//						}
//					}
//				}
//			}
//		}
//		internalCardSets.sort(null);
//
////		final ArrayList<CardSet> cardSets = new ArrayList<>();
//		final CardSet[] cardSets = new CardSet[internalCardSets.size()];
//
//		int cardScore = 1;
//
//		InternalCardSet lastCardSet = internalCardSets.get(0);
//		cardSets[0] = new CardSet(lastCardSet.getCards(), lastCardSet.getRoyalty(), lastCardSet.isStraight(), cardScore);
//
//		for (int cardSetsSize = 1; cardSetsSize < cardSets.length; cardSetsSize++) {
//			InternalCardSet cardSet = internalCardSets.get(cardSetsSize);
//			if (cardSet.compareTo(lastCardSet) != 0) cardScore++;
//			lastCardSet = cardSet;
////			cardSets.add(new CardSet(cardSet.getCards(), cardSet.getRoyalty(), cardSet.isStraight(), cardScore));
//			cardSets[cardSetsSize] = new CardSet(cardSet.getCards(), cardSet.getRoyalty(), cardSet.isStraight(), cardScore);
//		}
////		cardSets.sort(CardSet::compareByPrimitive);
//		Arrays.sort(cardSets, CardSet::compareByPrimitive);
////		return cardSets.toArray(new CardSet[0]);
//
//		return cardSets;
//	}

	private static CardSet[] createCardSets() {
		int internalCardSetsIndex = 0;
		final InternalCardSet[] internalCardSets = new InternalCardSet[2621060];
		for (int cardA = 0; cardA < Card.LENGTH; cardA++) {
			for (int cardB = cardA + 1; cardB < Card.LENGTH; cardB++) {
				for (int cardC = cardB + 1; cardC < Card.LENGTH; cardC++) {
					// add Trinity
					internalCardSets[internalCardSetsIndex++] = InternalCardSet.of(cardA, cardB, cardC);

					for (int cardD = cardC + 1; cardD < Card.LENGTH; cardD++) {
						for (int cardE = cardD + 1; cardE < Card.LENGTH; cardE++) {
							// add Quintet
							internalCardSets[internalCardSetsIndex++] = InternalCardSet.of(cardA, cardB, cardC, cardD, cardE);
						}
					}
				}
			}
		}
		Arrays.sort(internalCardSets);

		final CardSet[] cardSets = new CardSet[internalCardSets.length];

		int cardScore = 1;

		InternalCardSet lastCardSet = internalCardSets[0];
		cardSets[0] = new CardSet(lastCardSet.getCards(), lastCardSet.getRoyalty(), lastCardSet.isStraight(), cardScore);

		for (int cardSetsSize = 1; cardSetsSize < cardSets.length; cardSetsSize++) {
			InternalCardSet cardSet = internalCardSets[cardSetsSize];
			if (cardSet.compareTo(lastCardSet) != 0) cardScore++;
			lastCardSet = cardSet;
			cardSets[cardSetsSize] = new CardSet(cardSet.getCards(), cardSet.getRoyalty(), cardSet.isStraight(), cardScore);
		}
		Arrays.sort(cardSets, CardSet::compareByPrimitive);

		return cardSets;
	}

	private static CardSet getByPrimitive(long cards) {
		int low = 0;
		int high = CARD_SETS.length - 1;

		while (low <= high) {
			final int mid = (low + high) >> 1;
			final CardSet cardSet = CARD_SETS[mid];
			final int cmp = Cards.compare(cards, cardSet.cards);
			if (cmp > 0) {
				low = mid + 1;
			} else if (cmp < 0) {
				high = mid - 1;
			} else {
				return cardSet;
			}
		}
		return null;
	}

	private static int compareByPrimitive(CardSet cardSetA, CardSet cardSetB) {
		return Cards.compare(cardSetA.cards, cardSetB.cards);
	}

	public long getCards() {
		return cards;
	}

	public int getRoyalty() {
		return (scoreRoyaltyStraight & 0xFFFF) >> 1;
	}

	public boolean isQuintet() {
		return Cards.size(cards) == 5;
	}

	public boolean isTrinity() {
		return Cards.size(cards) == 3;
	}

	public boolean isStraight() {
		return (scoreRoyaltyStraight & 1) != 0;
	}

	public boolean isFlush() {
		return Cards.isSameSuit(cards);
	}

	@Override
	public boolean equals(Object object) {
		return object == this || object instanceof CardSet && Cards.equals(this.cards, ((CardSet) object).cards);
	}

	@Override
	public int hashCode() {
		return Cards.hashCode(cards);
	}

	@Override
	public int compareTo(CardSet cardSet) {
		return Integer.compare(this.scoreRoyaltyStraight & 0xFFFF0000, cardSet.scoreRoyaltyStraight & 0xFFFF0000);
	}

	@Override
	public String toString() {
		return "{" + Cards.toString(cards) + ", " + Royalty.toString(getRoyalty()) + ", " + (scoreRoyaltyStraight >> 16) + "}";
	}
}

