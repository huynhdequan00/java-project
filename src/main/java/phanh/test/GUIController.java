package phanh.test;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import mrmathami.chinesepoker.computer.ComputerBrain;
import mrmathami.chinesepoker.world.game.PlayerOrder;
import mrmathami.chinesepoker.world.game.*;
import mrmathami.utilities.Timer;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

public final class GUIController implements Initializable {
	private final Game game;

	@FXML public GridPane mainScene;
	@FXML public AnchorPane mainSceneHider;
	@FXML public MainSceneController mainSceneController;
	@FXML public GridPane sortingScene;
	@FXML public AnchorPane sortingSceneHider;
	@FXML public SortingSceneController sortingSceneController;


	private HumanBrain humanBrain;
	private GameInstance gameInstance;
	private Timer timer;

	public GUIController() {
		this.humanBrain = new HumanBrain();
		this.game = Game.of(60, TimeUnit.SECONDS,
				Player.of(ComputerBrain::play, 10000),
				Player.of(ComputerBrain::play, 10000),
				Player.of(ComputerBrain::play, 10000),
				Player.of(humanBrain, 10000)
		);
	}

	@Override
	public void initialize(URL url, ResourceBundle rb) {
		mainSceneController.setGuiController(this);
		sortingSceneController.setGuiController(this);
		sortingSceneHider.setVisible(false);
	}


	public void onStartAction() {
		this.gameInstance = game.createInstance(this::onStartCallback, this::onTickCallback, this::onFinishCallback);
		gameInstance.start();
	}

	public void onFinishAction() {
		humanBrain.setResult(sortingSceneController.mainHandController.getHandSet());
		humanBrain.interrupt();
	}

	private void onStartCallback() {
		mainSceneHider.setVisible(false);
		sortingSceneHider.setVisible(true);
		this.timer = Timer.of(60, TimeUnit.SECONDS);
		sortingSceneController.countDown.setText(timer.getRemainingTime(TimeUnit.SECONDS) + "s");
		final TableHands tableHands = gameInstance.getTableHands();
		sortingSceneController.firstHandController.setCards(tableHands.get(PlayerOrder.PLAYER_1));
		sortingSceneController.secondHandController.setCards(tableHands.get(PlayerOrder.PLAYER_2));
		sortingSceneController.thirdHandController.setCards(tableHands.get(PlayerOrder.PLAYER_3));
		sortingSceneController.mainHandController.setCards(tableHands.get(PlayerOrder.PLAYER_4));
	}

	private void onTickCallback() {
		Platform.runLater(() -> {
			sortingSceneController.countDown.setText(timer.getRemainingTime(TimeUnit.SECONDS) + "s");
		});
	}

	private void onFinishCallback() {
		onFinishAction();
		Platform.runLater(() -> {
			sortingSceneHider.setVisible(false);
			mainSceneHider.setVisible(true);
			final TableHandSets tableHandSets = gameInstance.getHandSets();
			mainSceneController.firstHandController.setCards(tableHandSets.get(PlayerOrder.PLAYER_1));
			mainSceneController.secondHandController.setCards(tableHandSets.get(PlayerOrder.PLAYER_2));
			mainSceneController.thirdHandController.setCards(tableHandSets.get(PlayerOrder.PLAYER_3));
			mainSceneController.mainHandController.setCards(tableHandSets.get(PlayerOrder.PLAYER_4));
			final TableDeltas tableDeltas = gameInstance.getDeltas();
			mainSceneController.firstDeltaScore.setText(tableDeltas.get(PlayerOrder.PLAYER_1).toString());
			mainSceneController.secondDeltaScore.setText(tableDeltas.get(PlayerOrder.PLAYER_2).toString());
			mainSceneController.thirdDeltaScore.setText(tableDeltas.get(PlayerOrder.PLAYER_3).toString());
			mainSceneController.mainDeltaScore.setText(tableDeltas.get(PlayerOrder.PLAYER_4).toString());
			final Players players = game.getPlayers();
			mainSceneController.firstScore.setText(Long.toString(players.get(PlayerOrder.PLAYER_1).getScore()));
			mainSceneController.secondScore.setText(Long.toString(players.get(PlayerOrder.PLAYER_2).getScore()));
			mainSceneController.thirdScore.setText(Long.toString(players.get(PlayerOrder.PLAYER_3).getScore()));
			mainSceneController.mainScore.setText(Long.toString(players.get(PlayerOrder.PLAYER_4).getScore()));
		});
	}
}


