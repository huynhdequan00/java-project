package phanh.test;

import mrmathami.chinesepoker.world.base.HandSet;
import mrmathami.chinesepoker.world.game.PlayerOrder;
import mrmathami.chinesepoker.world.game.PlayerBrain;
import mrmathami.chinesepoker.world.game.PlayerResult;
import mrmathami.chinesepoker.world.game.TableHands;
import mrmathami.utilities.Timer;

public final class HumanBrain implements PlayerBrain {
	private boolean isFinished = false;
	private HandSet result;

	public synchronized void interrupt() {
		isFinished = true;
	}

	public synchronized void reset() {
		this.isFinished = false;
	}

	public synchronized boolean isInterrupted() {
		return isFinished;
	}

	public void setResult(HandSet result) {
		this.result = result;
	}

	@Override
	public void play(TableHands tableHands, PlayerOrder playerOrder, Timer timer, PlayerResult result) {
		reset();
		// yup, human do human thing, and it takes times
		try {
			while (!isInterrupted() && !timer.isExpired()) Thread.sleep(1000);
			result.setResult(this.result);
		} catch (InterruptedException ignored) {
		}
		System.out.println("interrupted");
	}
}
