package mrmathami.chinesepoker.smallworld.game;

import mrmathami.chinesepoker.smallworld.base.Card;
import mrmathami.chinesepoker.smallworld.base.Cards;
import mrmathami.utilities.enumerate.EnumArray;
import mrmathami.utilities.enumerate.ReadOnlyEnumArray;

import java.util.Arrays;
import java.util.Random;

public final class TableHands extends ReadOnlyEnumArray<PlayerOrder, Long> {
	private TableHands(EnumArray<PlayerOrder, Long> enumArray) {
		super(enumArray);
	}

	public static TableHands generateTableHands(Random randomSource) {
		final EnumArray<PlayerOrder, Long> enumArray = new EnumArray<>(PlayerOrder.class, Long.class);
//		List<Integer> integerList = Arrays.stream(Card.values()).boxed().collect(Collectors.toList());
//		Collections.shuffle(integerList, randomSource);
//		final int[] deck = integerList.stream().mapToInt(Integer::intValue).toArray();
		final int[] deck = Card.values();
		final int[][] hands = new int[4][13];
		// Shuffle deck
		for (int i = 0; i < 256; i++) {
			int length = deck.length - 1;
			while (length >= 0) {
				final int random = randomSource.nextInt(length + 1);
				final int card = deck[random];
				deck[random] = deck[length];
				deck[length] = card;
				length -= 1;
			}
		}
		// Shuffle hands
		int length = deck.length - 1;
		while (length >= 0) {
			final int random = randomSource.nextInt(length + 1);
			hands[length & 3][length >> 2] = deck[random];
			deck[random] = deck[length--];
		}
		for (final PlayerOrder playerOrder : PlayerOrder.values) {
			enumArray.put(playerOrder, Cards.of(hands[playerOrder.ordinal()]));
		}
		return new TableHands(enumArray);
	}
}

/*
public final class TableHands {
	private final Cards[] hands;

	private TableHands(Cards... hands) {
		assert hands.length == 4
				&& hands[0].size() == 13 && hands[1].size() == 13
				&& hands[2].size() == 13 && hands[3].size() == 13;
		this.hands = hands;
	}

	@Deprecated
	public static TableHands generateTableHands(Cards... hands) {
		return new TableHands(hands);
	}

	public static TableHands generateTableHands(Random randomSource) {
		final Card[] DECK = Card.values.clone();
		// Shuffle array
		for (int index = DECK.length - 1; index >= 0; index--) {
			final int random = randomSource.nextInt(index + 1);
			final Card card = DECK[index];
			DECK[index] = DECK[random];
			DECK[random] = card;
		}
		final Cards[] hands = new Cards[4];
		final Card[] hand = new Card[13];
		for (int i = 0; i < hands.length; i++) {
			System.arraycopy(DECK, i * 13, hand, 0, 13);
			hands[i] = Cards.generateTableHands(hand);
		}
		return new TableHands(hands);
	}

	public Cards getCards(PlayerOrder playerOrder) {
		return hands[playerOrder.ordinal()];
	}

	@Override
	public String toString() {
		return Arrays.toString(hands);
	}

	@Override
	public boolean equals(Object obj) {
		return this == obj || (obj instanceof TableHands && Arrays.equals(hands, ((TableHands) obj).hands));
	}

	@Override
	public int hashCode() {
		return Arrays.hashCode(hands);
	}
}
*/
