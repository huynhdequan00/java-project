package mrmathami.utilities.enumerate;

public class ReadOnlyEnumArray<EnumType extends Enum<EnumType>, ValueType> extends EnumArray<EnumType, ValueType> {
	public ReadOnlyEnumArray(EnumArray<EnumType, ValueType> enumArray) {
		super(enumArray);
	}

	@Override
	public void put(EnumType key, ValueType value) {
		throw new UnsupportedOperationException();
	}
}
