package mrmathami.chinesepoker.smallworld.base;

public final class Cards {
	private static final long MASK_ALL = 0x000F_FFFF_FFFF_FFFFL;
	private static final long MASK_RED = 0x0006_6666_6666_6666L;
	private static final long MASK_BLACK = 0x0009_9999_9999_9999L;
	private static final long[] MASK_SUIT = {
			0x0001_1111_1111_1111L,
			0x0002_2222_2222_2222L,
			0x0004_4444_4444_4444L,
			0x0008_8888_8888_8888L
	};
	private static final long[] MASK_RANK = {
			0x0000_0000_0000_000FL,
			0x0000_0000_0000_00F0L,
			0x0000_0000_0000_0F00L,
			0x0000_0000_0000_F000L,
			0x0000_0000_000F_0000L,
			0x0000_0000_00F0_0000L,
			0x0000_0000_0F00_0000L,
			0x0000_0000_F000_0000L,
			0x0000_000F_0000_0000L,
			0x0000_00F0_0000_0000L,
			0x0000_0F00_0000_0000L,
			0x0000_F000_0000_0000L,
			0x000F_0000_0000_0000L,
	};

	private Cards() {
	}

	public static String toString(long cards) {
		if (cards == 0) return "[]";

		final int size = Long.bitCount(cards & MASK_ALL);
		final StringBuilder builder = new StringBuilder().append('[');
		for (int i = 0; i < size; i++) {
			if (i != 0) builder.append(',');
			int card = Long.numberOfTrailingZeros(cards);
			builder.append(Card.toString(card));
			cards ^= 1L << card;
		}
		return builder.append(']').toString();
	}

	public static long mask(long cards) {
		return cards & MASK_ALL;
	}

	public static int size(long cards) {
		return Long.bitCount(cards & MASK_ALL);
	}

	public static int hashCode(long cards) {
		return Long.hashCode(cards & MASK_ALL);
	}

	public static long of(int... cards) {
		long mask = 0;
		for (int card : cards) {
			mask |= 1L << card;
		}
		return mask;
	}

	public static long of(long... cardsList) {
		int size = 0;
		long mask = 0;
		for (long cards : cardsList) {
			size += size(cards);
			mask |= cards;
		}
		assert size(mask) != size;
		return mask;
	}

	public static boolean isSameSuit(long cards) {
		return (cards & MASK_SUIT[0]) == cards || (cards & MASK_SUIT[1]) == cards || (cards & MASK_SUIT[2]) == cards || (cards & MASK_SUIT[3]) == cards;
	}

	public static int countRank(long cards, int rank) {
		return Long.bitCount(cards & MASK_RANK[rank]);
	}

	public static int countRanks(long cards) {
		int countRanks = 0;
		for (int rank = 0; rank < Rank.LENGTH; rank++) if ((cards & MASK_RANK[rank]) != 0) countRanks++;
		return countRanks;
	}

	public static boolean equals(long cardsA, long cardsB) {
		return mask(cardsA) == mask(cardsB);
	}

	public static int compare(long cardsA, long cardsB) {
		return Long.compare(mask(cardsA), mask(cardsB));
	}

	public static int countRed(long cards) {
		return Long.bitCount(cards & MASK_RED);
	}

	public static int[] toArray(long cards) {
		final int size = Long.bitCount(cards);
		final int[] cardArray = new int[size];
		for (int i = 0; i < size; i++) {
			final int ordinal = Long.numberOfTrailingZeros(cards);
			cardArray[i] = ordinal;
			cards ^= 1L << ordinal;
		}
		return cardArray;
	}
}
