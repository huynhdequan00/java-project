package mrmathami.chinesepoker.smallworld.computer;

import mrmathami.chinesepoker.smallworld.base.Cards;
import mrmathami.chinesepoker.smallworld.base.HandRoyalty;
import mrmathami.chinesepoker.smallworld.base.HandSet;
import mrmathami.chinesepoker.smallworld.game.PlayerOrder;
import mrmathami.chinesepoker.smallworld.game.TableHands;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;

final class PlayerHandSetArray implements Callable<PlayerHandSetArray> {
	private final PlayerOrder playerOrder;
	private final int[] cardArray;
	private final List<HandSet> handSets;
	private final int[] top = new int[3];
	private final int[] mid = new int[5];
	private final int[] bottom = new int[5];

	private PlayerHandSetArray(PlayerOrder playerOrder, int[] cardArray) {
		this.playerOrder = playerOrder;
		this.cardArray = cardArray;
		this.handSets = new LinkedList<>();
	}

	static PlayerHandSetArray of(TableHands tableHands, PlayerOrder playerOrder) {
		return new PlayerHandSetArray(playerOrder, Cards.toArray(tableHands.get(playerOrder)));
	}

	PlayerOrder getPlayerOrder() {
		return playerOrder;
	}

	HandSet[] getHandSetArray() {
		return handSets.toArray(new HandSet[0]);
	}

	@Override
	public PlayerHandSetArray call() {
		recursiveCall(0, 0, 0, 0);
		return this;
	}

	private static boolean shouldAdd(List<HandSet> handSets, HandSet myHandSet) {
		if (myHandSet.getHandRoyalty() == HandRoyalty.MIS_SET) return false;
		boolean shouldAdd = true;
		for (Iterator<HandSet> iterator = handSets.iterator(); iterator.hasNext(); ) {
			HandSet handSet = iterator.next();
			Integer weakCompareTo = myHandSet.weakCompareTo(handSet);
			if (weakCompareTo != null) {
				if (weakCompareTo < 0) {
					shouldAdd = false;
					break;
				} else if (weakCompareTo == 0) {
					shouldAdd = false;
				} else {
					iterator.remove();
				}
			}
		}
		return shouldAdd;
	}

	private void recursiveCall(int cardIndex, int topSize, int midSize, int bottomSize) {
		if (cardIndex >= 13) {
			if (topSize == 3 && midSize == 5 && bottomSize == 5) {
				final HandSet myHandSet = HandSet.of(top, mid, bottom);
				if (shouldAdd(handSets, myHandSet)) handSets.add(myHandSet);
				if (HandRoyalty.isWhiteWin(myHandSet.getHandRoyalty())) {
					final HandSet myLowHandSet = HandSet.noWhiteWinOf(myHandSet);
					if (shouldAdd(handSets, myLowHandSet)) handSets.add(myLowHandSet);
				}
			}
		} else {
			if (topSize < 3) {
				top[topSize] = cardArray[cardIndex];
				recursiveCall(cardIndex + 1, topSize + 1, midSize, bottomSize);
			}
			if (midSize < 5) {
				mid[midSize] = cardArray[cardIndex];
				recursiveCall(cardIndex + 1, topSize, midSize + 1, bottomSize);
			}
			if (bottomSize < 5) {
				bottom[bottomSize] = cardArray[cardIndex];
				recursiveCall(cardIndex + 1, topSize, midSize, bottomSize + 1);
			}
		}
	}
}
