package mrmathami.chinesepoker.world.base;

import mrmathami.utilities.WeakComparable;

public final class HandSet implements WeakComparable<HandSet> {
	public static final HandSet EMPTY = new HandSet(null, null, null, HandRoyalty.MIS_SET);
	private final CardSet top;
	private final CardSet mid;
	private final CardSet bottom;
	private final HandRoyalty handRoyalty;

	private HandSet(CardSet top, CardSet mid, CardSet bottom, HandRoyalty handRoyalty) {
		this.top = top;
		this.mid = mid;
		this.bottom = bottom;
		this.handRoyalty = handRoyalty;
	}

	public static HandSet of(CardSet top, CardSet mid, CardSet bottom) {
		assert top.isTrinity() && mid.isQuintet() && bottom.isQuintet();
		return new HandSet(top, mid, bottom, createHandRoyalty(top, mid, bottom));
	}

	public static HandSet of(Cards top, Cards mid, Cards bottom) {
		return of(CardSet.of(top), CardSet.of(mid), CardSet.of(bottom));
	}

	public static HandSet of(Card[] top, Card[] mid, Card[] bottom) {
		return of(CardSet.of(top), CardSet.of(mid), CardSet.of(bottom));
	}

	private static HandRoyalty createHandRoyalty(CardSet top, CardSet mid, CardSet bottom) {
		if (top.compareTo(mid) > 0 || mid.compareTo(bottom) > 0) return HandRoyalty.MIS_SET;

		final Cards cards = Cards.of(top.getCards(), mid.getCards(), bottom.getCards());
		if (cards.countRanks() == 13) return HandRoyalty.DRAGON_HAND;

		final int countRed = cards.countRed();
		if (countRed == 0 || countRed == 13) return HandRoyalty.COLOR_THIRTEEN;
		if (countRed == 1 || countRed == 12) return HandRoyalty.COLOR_TWELVE;

		if (top.isFlush() && mid.isFlush() && bottom.isFlush()) return HandRoyalty.THREE_FLUSHES;
		if (top.isStraight() && mid.isStraight() && bottom.isStraight()) return HandRoyalty.THREE_STRAIGHTS;

		final RankCounters counters = RankCounters.of(cards);
		if (counters.size() == 7 && counters.getCount(0) == 2
				|| counters.size() == 6 && (counters.getCount(0) == 3 || counters.getCount(0) == 4) && counters.getCount(1) == 2
				|| counters.size() == 5 && counters.getCount(0) == 4 && counters.getCount(1) == 3 && counters.getCount(2) == 2) {
			return HandRoyalty.SIX_PAIRS;
		}

		return HandRoyalty.NORMAL;
	}

	private static int compareTop(CardSet topA, CardSet topB) {
		assert topA.isTrinity() && topB.isTrinity();
		final int topCmp = topA.compareTo(topB);
		if (topCmp > 0) {
			final Royalty royaltyA = topA.getRoyalty();
			final Royalty royaltyB = topB.getRoyalty();
			if (royaltyA.equals(Royalty.THREE_CARDS) && royaltyB.compareTo(Royalty.THREE_CARDS) < 0) {
				// Sám chi cuối = 6 cược
				return 3;
			}
			return 1;
		} else if (topCmp < 0) {
			return -compareTop(topB, topA);
		}
		return 0;
	}

	private static int compareMid(CardSet midA, CardSet midB) {
		final int midCmp = midA.compareTo(midB);
		if (midCmp > 0) {
			final Royalty royaltyA = midA.getRoyalty();
			final Royalty royaltyB = midB.getRoyalty();
			if (royaltyA.compareTo(Royalty.BABY_STRAIGHT_FLUSH) >= 0 && royaltyB.compareTo(Royalty.BABY_STRAIGHT_FLUSH) < 0) {
				// Thùng phá sảnh chi giữa = 20 cược
				return 10;
			} else if (royaltyA.equals(Royalty.FOUR_CARDS) && royaltyB.compareTo(Royalty.FOUR_CARDS) < 0) {
				// Tứ quý chi giữa = 16 cược
				return 8;
			} else if (royaltyA.equals(Royalty.FULL_HOUSE) && royaltyB.compareTo(Royalty.FULL_HOUSE) < 0) {
				// Cù lũ chi giữa = 4 cược
				return 2;
			}
			return 1;
		} else if (midCmp < 0) {
			return -compareTop(midB, midA);
		}
		return 0;
	}

	private static int compareBottom(CardSet bottomA, CardSet bottomB) {
		final int bottomCmp = bottomA.compareTo(bottomB);
		if (bottomCmp > 0) {
			final Royalty royaltyA = bottomA.getRoyalty();
			final Royalty royaltyB = bottomB.getRoyalty();
			if (royaltyA.compareTo(Royalty.BABY_STRAIGHT_FLUSH) >= 0 && royaltyB.compareTo(Royalty.BABY_STRAIGHT_FLUSH) < 0) {
				// Thùng phá sảnh chi đầu = 10 cược
				return 5;
			} else if (royaltyA.equals(Royalty.FOUR_CARDS) && royaltyB.compareTo(Royalty.FOUR_CARDS) < 0) {
				// Tứ quý chi đầu = 8 cược
				return 4;
			}
			return 1;
		} else if (bottomCmp < 0) {
			return -compareTop(bottomB, bottomA);
		}
		return 0;
	}

	public CardSet getTop() {
		return top;
	}

	public CardSet getMid() {
		return mid;
	}

	public CardSet getBottom() {
		return bottom;
	}

	@Override
	public Integer weakCompareTo(HandSet handSet) {
		if (handSet == this) return 0;

		final int compareTo = this.handRoyalty.compareTo(handSet.handRoyalty);
		if (compareTo != 0) return compareTo;

		final int topCmp = this.top.compareTo(handSet.top);
		final int midCmp = this.mid.compareTo(handSet.mid);
		final int bottomCmp = this.bottom.compareTo(handSet.bottom);

		if (topCmp == 0 && midCmp == 0 && bottomCmp == 0) {
			return 0;
		} else if (topCmp >= 0 && midCmp >= 0 && bottomCmp >= 0) {
			return 1;
		} else if (topCmp <= 0 && midCmp <= 0 && bottomCmp <= 0) {
			return -1;
		}
		return null;
	}

	public int compareNormalHand(HandSet handSet) {
		final int topCmp = this.top.compareTo(handSet.top);
		final int midCmp = this.mid.compareTo(handSet.mid);
		final int bottomCmp = this.bottom.compareTo(handSet.bottom);

		final int topScore = compareTop(this.top, handSet.top);
		final int midScore = compareTop(this.mid, handSet.mid);
		final int bottomScore = compareTop(this.bottom, handSet.bottom);

		return (topScore + midScore + bottomScore) << (topCmp == midCmp && midCmp == bottomCmp ? 1 : 0);
	}

	// TODO: TEST ME: compare and return score (win when bigger than 0)
	public int compareHand(HandSet handSet) {
		if (this.handRoyalty.isWhiteWin() && handSet.handRoyalty.isWhiteWin() || this.handRoyalty == HandRoyalty.NORMAL && handSet.handRoyalty == HandRoyalty.NORMAL) {
			return compareNormalHand(handSet);
		}
		if (this.handRoyalty.compareTo(handSet.handRoyalty) > 0) {
			switch (this.handRoyalty) {
				case NORMAL:
				case SIX_PAIRS:
				case THREE_STRAIGHTS:
				case THREE_FLUSHES:
					return 6;

				case COLOR_TWELVE:
					return 8;

				case COLOR_THIRTEEN:
					return 10;

				case DRAGON_HAND:
					return 24;
			}
		} else {
			switch (handSet.handRoyalty) {
				case NORMAL:
				case SIX_PAIRS:
				case THREE_STRAIGHTS:
				case THREE_FLUSHES:
					return -6;

				case COLOR_TWELVE:
					return -8;

				case COLOR_THIRTEEN:
					return -10;

				case DRAGON_HAND:
					return -24;
			}
		}
		assert this.handRoyalty == handSet.handRoyalty && this.handRoyalty == HandRoyalty.MIS_SET;
		return 0;
	}

	@Override
	public String toString() {
		return "{" + top + ", " + mid + ", " + bottom + ", " + handRoyalty + "}";
	}
}
