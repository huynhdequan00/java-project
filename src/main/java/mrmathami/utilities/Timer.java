package mrmathami.utilities;

import java.util.concurrent.TimeUnit;

public final class Timer {
	private final long duration;
	private final long start;
	private final long stop;

	private Timer(long duration, TimeUnit timeUnit) {
		this.duration = timeUnit.toNanos(duration);
		this.start = System.nanoTime();
		this.stop = this.start + this.duration;
	}

	public static Timer of(long duration, TimeUnit timeUnit) {
		return new Timer(duration, timeUnit);
	}

	public long getDuration(TimeUnit timeUnit) {
		return timeUnit.convert(duration, TimeUnit.NANOSECONDS);
	}

	public long getElapsedTime(TimeUnit timeUnit) {
		return timeUnit.convert(System.nanoTime() - start, TimeUnit.NANOSECONDS);
	}

	public long getRemainingTime(TimeUnit timeUnit) {
		return timeUnit.convert(stop - System.nanoTime(), TimeUnit.NANOSECONDS);
	}

	public boolean isExpired() {
		return stop - System.nanoTime() < 0;
	}
}
