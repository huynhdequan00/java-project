package mrmathami.chinesepoker.smallworld.base;

public final class Suit {
	public static final int INVALID = -1;
	// region
	public static final int SUIT_CLUB = 0;
	public static final int SUIT_DIAMOND = 1;
	public static final int SUIT_HEART = 2;
	public static final int SUIT_SPADE = 3;
	// endregion
	public static final int LENGTH = 4;

	private static final int[] values = {0, 1, 2, 3};
	private static final String[] RANK_NAME_TABLE = {"♣", "♦", "♥", "♠"};

	private Suit() {
	}

	public static int[] values() {
		return values.clone();
	}

	public static boolean isValid(int suitNum) {
		return suitNum >= 0 && suitNum < LENGTH;
	}

	public static String toString(int suitNum) {
		// should be an assertion, this function is internal use only
		assert isValid(suitNum) : "suitNum out of range! suitNum = " + suitNum;
		return RANK_NAME_TABLE[suitNum];
	}

	public static boolean isRed(int suitNum) {
		// should be an assertion, this function is internal use only
		assert isValid(suitNum) : "suitNum out of range! suitNum = " + suitNum;
		return suitNum == SUIT_DIAMOND || suitNum == SUIT_HEART;
	}

	public static boolean isBlack(int suitNum) {
		// should be an assertion, this function is internal use only
		assert isValid(suitNum) : "suitNum out of range! suitNum = " + suitNum;
		return suitNum == SUIT_CLUB || suitNum == SUIT_SPADE;
	}
}
