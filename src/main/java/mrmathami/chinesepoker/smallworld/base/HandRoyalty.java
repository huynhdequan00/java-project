package mrmathami.chinesepoker.smallworld.base;

public final class HandRoyalty {
	public static final int INVALID = -1;
	// region
	public static final int MIS_SET = 0;
	public static final int NORMAL = 1;
	public static final int SIX_PAIRS = 2;
	public static final int THREE_STRAIGHTS = 3;
	public static final int THREE_FLUSHES = 4;
	public static final int COLOR_TWELVE = 5;
	public static final int COLOR_THIRTEEN = 6;
	public static final int DRAGON_HAND = 7;
	// endregion
	public static final int LENGTH = 8;

	public static final int[] values = {0, 1, 2, 3, 4, 5, 6, 7};
	private static final String[] HAND_ROYALTY_STRING_TABLE = {
			"Mis-set", "Normal", "Six pairs", "Three straights",
			"Three flushes", "Twelve same color cards",
			"Thirteen same color cards", "Dragon hand"
	};

	private HandRoyalty() {
	}

	public static int[] values() {
		return values.clone();
	}

	public static boolean isValid(int handRoyaltyNum) {
		return handRoyaltyNum >= 0 && handRoyaltyNum < LENGTH;
	}

	public static String toString(int handRoyaltyNum) {
		// should be an assertion, this function is internal use only
		assert isValid(handRoyaltyNum) : "handRoyaltyNum out of range! handRoyaltyNum = " + handRoyaltyNum;
		return HAND_ROYALTY_STRING_TABLE[handRoyaltyNum];
	}

	public static boolean isWhiteWin(int handRoyaltyNum) {
		// should be an assertion, this function is internal use only
		assert isValid(handRoyaltyNum) : "handRoyaltyNum out of range! handRoyaltyNum = " + handRoyaltyNum;
		return handRoyaltyNum > NORMAL;
	}

}
