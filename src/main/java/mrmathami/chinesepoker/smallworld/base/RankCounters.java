package mrmathami.chinesepoker.smallworld.base;

import java.util.Arrays;

final class RankCounters {
	private RankCounters() {
	}

	public static int[] of(long cards) {
		final int[] tmpCounters = new int[Cards.size(cards)];
		int size = 0;
		//Rank[] values = Rank.values;
		for (int rank = 0; rank < Rank.LENGTH; rank++) {
			final int count = Cards.countRank(cards, rank);
			if (count > 0) tmpCounters[size++] = rank | (count << 16);
		}

		int[] counters = new int[size];
		for (int counterIndex = 0; counterIndex < size; counterIndex++) {
			int maxIndex = 0;
			for (int tmpIndex = 0; tmpIndex < size; tmpIndex++) {
				if (tmpCounters[maxIndex] < tmpCounters[tmpIndex]) maxIndex = tmpIndex;
			}
			counters[counterIndex] = tmpCounters[maxIndex];
			tmpCounters[maxIndex] = 0;
		}
		return counters;
	}

	public static int size(int[] counters) {
		return counters.length;
	}

	public static int getCount(int[] counters, int index) {
		return counters[index] >> 16;
	}

	public static int getRank(int[] counters, int index) {
		return counters[index] & 0xFFFF;
	}

	public static int compare(int[] countersA, int[] countersB) {
		return Arrays.compare(countersA, countersB);
	}
}
