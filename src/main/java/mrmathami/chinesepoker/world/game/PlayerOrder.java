package mrmathami.chinesepoker.world.game;

public enum PlayerOrder {
	PLAYER_1("Player 1"),
	PLAYER_2("Player 2"),
	PLAYER_3("Player 3"),
	PLAYER_4("Player 4");

	public static final PlayerOrder[] values = PlayerOrder.values();
	private final String string;

	PlayerOrder(String string) {
		this.string = string;
	}

	public static PlayerOrder next(PlayerOrder playerOrder) {
		return values[(playerOrder.ordinal() + 1) & 3];
	}

	public static PlayerOrder prev(PlayerOrder playerOrder) {
		return values[(playerOrder.ordinal() - 1) & 3];
	}

	public PlayerOrder next() {
		return values[(this.ordinal() + 1) & 3];
	}

	public PlayerOrder prev() {
		return values[(this.ordinal() - 1) & 3];
	}

	@Override
	public String toString() {
		return this.string;
	}
}
