package mrmathami.chinesepoker.smallworld.computer;

import mrmathami.chinesepoker.smallworld.base.HandSet;
import mrmathami.chinesepoker.smallworld.game.PlayerOrder;
import mrmathami.chinesepoker.smallworld.game.PlayerResult;
import mrmathami.chinesepoker.smallworld.game.TableHands;
import mrmathami.utilities.Timer;

public final class ComputerBrain {
	private static TableHandSetArray tableHandSetArray;

	private synchronized static TableHandSetArray createTableHandSetArray(TableHands tableHands) throws InterruptedException {
		if (tableHandSetArray == null || !tableHandSetArray.getTableHands().equals(tableHands)) {
			tableHandSetArray = TableHandSetArray.of(tableHands);
		}
		return tableHandSetArray;
	}

	public static void play(TableHands tableHands, PlayerOrder playerOrder, Timer timer, PlayerResult result) {
		try {
			final TableHandSetArray tableHandSetArray = createTableHandSetArray(tableHands);

			HandSet resultHandSet = null;
			double resultWinCoverage = 0.0;
			double resultLossSummarized = 0;
			double resultWinSummarized = 0;

			final HandSet[] myHandSets = tableHandSetArray.getPlayerHandSetArray(playerOrder);
			for (final HandSet myHandSet : myHandSets) {
				double currentWinCoverage = 0.0;
				double currentLossSummarized = 0;
				double currentWinSummarized = 0;

				for (final PlayerOrder opponentPlayerOrder : PlayerOrder.values) {
					if (opponentPlayerOrder != playerOrder) {
						int currentOpponentWinCoverage = 0;
						int currentOpponentLossCoverage = 0;
						int currentOpponentLoss = 0;
						int currentOpponentWin = 0;

						final HandSet[] opponentHandSets = tableHandSetArray.getPlayerHandSetArray(opponentPlayerOrder);
						for (final HandSet opponentHandSet : opponentHandSets) {
							final int compareHand = myHandSet.compareTo(opponentHandSet);
							if (compareHand >= 0) {
								currentOpponentWin += compareHand;
								currentOpponentWinCoverage += 1;
							} else {
								currentOpponentLoss += compareHand;
								currentOpponentLossCoverage += 1;
							}
						}
						currentWinCoverage += (double) currentOpponentWinCoverage / opponentHandSets.length;
						currentLossSummarized += (double) currentOpponentLoss / (currentOpponentLossCoverage != 0 ? currentOpponentLossCoverage : 1);
						currentWinSummarized += (double) currentOpponentWin / (currentOpponentWinCoverage != 0 ? currentOpponentWinCoverage : 1);
					}
				}
				if (
						currentWinCoverage > resultWinCoverage
								|| currentWinCoverage == resultWinCoverage && currentLossSummarized > resultLossSummarized
								|| currentWinCoverage == resultWinCoverage && currentLossSummarized == resultLossSummarized && currentWinSummarized > resultWinSummarized
				) {
					resultHandSet = myHandSet;
					resultWinCoverage = currentWinCoverage;
					resultLossSummarized = currentLossSummarized;
					resultWinSummarized = currentWinSummarized;
					result.setResult(myHandSet);
				}
			}
			// TODO
			System.out.println("result = " + resultHandSet + "\nresultWinCoverage = " + resultWinCoverage + "\nresultLossSummarized = " + resultLossSummarized + "\nresultWinSummarized = " + resultWinSummarized);
			//Thread.sleep(2000);
		} catch (InterruptedException ignored) {
		}
	}

	public static void play2(TableHands tableHands, PlayerOrder playerOrder, Timer timer, PlayerResult result) {
		try {
			final TableHandSetArray tableHandSetArray = createTableHandSetArray(tableHands);

			HandSet resultHandSet = null;
			double resultWinCoverage = 0.0;
			int resultWinSummarized = 0;
			int resultMaximumLoss = 0;

			final HandSet[] myHandSets = tableHandSetArray.getPlayerHandSetArray(playerOrder);
			for (final HandSet myHandSet : myHandSets) {
				double currentWinCoverage = 0.0;
				int currentWinSummarized = 0;
				int currentMaximumLoss = 0;

				for (final PlayerOrder opponentPlayerOrder : PlayerOrder.values) {
					if (opponentPlayerOrder != playerOrder) {
						int currentOpponentWinCoverage = 0;
						int currentOpponentWin = 0;

						final HandSet[] opponentHandSets = tableHandSetArray.getPlayerHandSetArray(opponentPlayerOrder);
						for (final HandSet opponentHandSet : opponentHandSets) {
							final int compareHand = myHandSet.compareTo(opponentHandSet);
							currentOpponentWin += compareHand;
							if (compareHand >= 0) {
								currentOpponentWinCoverage += 1;
							} else if (compareHand < currentMaximumLoss) {
								currentMaximumLoss = compareHand;
							}
						}
						currentWinCoverage += (double) currentOpponentWinCoverage / opponentHandSets.length;
						currentWinSummarized += (double) currentOpponentWin / (currentOpponentWinCoverage != 0 ? currentOpponentWinCoverage : 1);
					}
				}
				if (
						currentWinCoverage > resultWinCoverage
								|| currentWinCoverage == resultWinCoverage && currentMaximumLoss > resultMaximumLoss
								|| currentWinCoverage == resultWinCoverage && currentMaximumLoss == resultMaximumLoss && currentWinSummarized > resultWinSummarized
				) {
					resultHandSet = myHandSet;
					resultWinCoverage = currentWinCoverage;
					resultMaximumLoss = currentMaximumLoss;
					resultWinSummarized = currentWinSummarized;
					result.setResult(myHandSet);
				}
			}
			// TODO
			System.out.println("result = " + resultHandSet);
			//System.out.println("result = " + resultHandSet + "\nresultWinCoverage = " + resultWinCoverage + "\nresultMaximumLoss = " + resultMaximumLoss + "\nresultWinSummarized = " + resultWinSummarized);
			//Thread.sleep(2000);
		} catch (InterruptedException ignored) {
		}
	}


	public static void play_against_last(TableHands tableHands, PlayerOrder playerOrder, Timer timer, PlayerResult result) {
		try {
			HandSet resultHandSet = SingleSolver.solve(tableHands.get(playerOrder), tableHands.get(PlayerOrder.PLAYER_4));
			result.setResult(resultHandSet);

			// TODO
			System.out.println("result = " + resultHandSet);
			//System.out.println("result = " + resultHandSet + "\nresultWinCoverage = " + resultWinCoverage + "\nresultMaximumLoss = " + resultMaximumLoss + "\nresultWinSummarized = " + resultWinSummarized);
			//Thread.sleep(2000);
		} catch (InterruptedException ignored) {
		}
	}

}
