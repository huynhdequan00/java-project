package mrmathami.chinesepoker.smallworld.base;

import static mrmathami.chinesepoker.smallworld.base.RankCounters.getCount;
import static mrmathami.chinesepoker.smallworld.base.RankCounters.getRank;

final class InternalCardSet implements Comparable<InternalCardSet> {
	private final long straightRoyaltyCards;
	//		private final long cards;
//		private final int royalty;
//		private final boolean isStraight;
	private final int[] rankCounters;

	private InternalCardSet(long cards, int royalty, boolean isStraight, int[] rankCounters) {
		this.straightRoyaltyCards = cards + ((long) royalty << 52) + (isStraight ? (1L << 60) : 0L);
//			this.cards = cards;
//			this.royalty = royalty;
//			this.isStraight = isStraight;
		this.rankCounters = rankCounters;
	}

	static InternalCardSet of(int... cardArray) {
		assert cardArray.length == 3 || cardArray.length == 5;

		// createCardSets cards
		final long cards = Cards.of(cardArray);

		// createCardSets descending sorted rankCounters
		final int[] rankCounters = RankCounters.of(cards);

		// createCardSets royalty
		int royalty;
		boolean isStraight = false;

		if (Cards.size(cards) == 5) {
			if (RankCounters.size(rankCounters) == 5) {
				final boolean isBabyStraight = getRank(rankCounters, 0) == Rank.RANK_A
						&& getRank(rankCounters, 1) == Rank.RANK_5;
				final boolean isNormalStraight = getRank(rankCounters, 0) - getRank(rankCounters, 4) == 4;
				isStraight = isBabyStraight || isNormalStraight;
				royalty = Cards.isSameSuit(cards) ?
						(isNormalStraight ? Royalty.STRAIGHT_FLUSH :
								(isBabyStraight ? Royalty.BABY_STRAIGHT_FLUSH : Royalty.FLUSH)) :
						(isNormalStraight ? Royalty.STRAIGHT :
								(isBabyStraight ? Royalty.BABY_STRAIGHT : Royalty.HIGH_CARDS));

			} else if (RankCounters.size(rankCounters) == 4) {
				royalty = Royalty.ONE_PAIR;
			} else if (RankCounters.size(rankCounters) == 3) {
				royalty = getCount(rankCounters, 0) == 3 ? Royalty.THREE_CARDS : Royalty.TWO_PAIRS;
			} else {
				royalty = getCount(rankCounters, 0) == 4 ? Royalty.FOUR_CARDS : Royalty.FULL_HOUSE;
			}
		} else {
			if (RankCounters.size(rankCounters) == 3) {
				final boolean isBabyStraight = getRank(rankCounters, 0) == Rank.RANK_A
						&& getRank(rankCounters, 1) == Rank.RANK_3;
				final boolean isNormalStraight = getRank(rankCounters, 0) - getRank(rankCounters, 2) == 2;
				isStraight = isBabyStraight || isNormalStraight;
				royalty = Royalty.HIGH_CARDS;
			} else {
				royalty = RankCounters.size(rankCounters) == 2 ? Royalty.ONE_PAIR : Royalty.THREE_CARDS;
			}
		}

		return new InternalCardSet(cards, royalty, isStraight, rankCounters);
	}

	@Override
	public final int compareTo(InternalCardSet internalCardSet) {
		// same card set check
		if (Cards.equals(this.straightRoyaltyCards, internalCardSet.straightRoyaltyCards)) return 0;

		// compare royalty
		final int compareTo = Integer.compare(this.getRoyalty(), internalCardSet.getRoyalty());
		if (compareTo != 0) return compareTo;

		// specific to CountedQuintet
		final int royalty = this.getRoyalty();
		if (royalty == Royalty.BABY_STRAIGHT || royalty == Royalty.BABY_STRAIGHT_FLUSH) {
			return 0;
		} else if (royalty == Royalty.STRAIGHT || royalty == Royalty.STRAIGHT_FLUSH) {
			return Integer.compare(getRank(this.rankCounters, 0), getRank(internalCardSet.rankCounters, 0));
		}

		// compare counters
		return RankCounters.compare(this.rankCounters, internalCardSet.rankCounters);
	}

	final int getRoyalty() {
		return (int) (straightRoyaltyCards >> 52) & 0xFF;
	}

	final long getCards() {
		return Cards.mask(straightRoyaltyCards);
	}

	final boolean isStraight() {
		return (straightRoyaltyCards & (1L << 60)) != 0;
	}
}
